function A4_createPreprocStructure

% Copy Niis into the preprocessign folder. Create proper folder structure.
% For rest, we also copy the brain extracted T1 images that have been
% created in the task pipeline. For this to work, both the task folders and
% the rest folder have to be available on tardis.

% 180109 | written by JQK
% 180126 | adapted for STSWD rest
% 180222 | added OAs

if ismac
    pn.root	= '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/';
else
    pn.root	= '/home/mpib/LNDG/StateSwitch';
end

pn.root_task         = [pn.root, 'WIP/preproc/'];
pn.root_anat         = [pn.root, 'WIP_anat/preproc/'];
pn.root_rest         = [pn.root, 'WIP_rest/preproc/'];

pn.preprocData_task       = [pn.root, 'WIP/preproc/B_data/D_preproc/'];
pn.preprocData_rest       = [pn.root, 'WIP_rest/preproc/B_data/D_preproc/'];
    
%% IDs

% N = 44 (180102);
%IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% OA: N = 57 (180206; including 4 pilots);
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2142';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2253';'2254';'2255';'2258';'2261'};

fileTypes = {'REST'};
fileNames = {'rest'};

for indID = 1:numel(IDs)
    try
        mkdir([pn.preprocData_rest, IDs{indID}]);
        mkdir([pn.preprocData_rest, IDs{indID}, '/mri/']);
        for indType = 1
            disp(['Creating preproc structure: ',fileTypes{indType}, ' Subject ' IDs{indID}]);
            % designate paths
            pn.target = [pn.preprocData_rest, IDs{indID}, '/mri/',fileNames{indType}, '/'];
            mkdir(pn.target);
            fileName = ['sub-STSWD', IDs{indID}, '_' fileNames{indType}];
            fileToCopy = [pn.root_rest, 'B_data/B_nii/', fileName, '_bold.nii.gz'];
            fileTarget = [pn.target, IDs{indID}, '_', fileNames{indType}, '.nii.gz'];
            if exist(fileToCopy)
                copyfile(fileToCopy, fileTarget);
            end
        end
        % copy anatomical images from task preproc folder
        pn.target = [pn.preprocData_rest, IDs{indID}, '/mri/t1'];
        pn.source = [pn.preprocData_task, IDs{indID}, '/mri/t1'];
        copyfile(pn.source, pn.target);
    catch disp(['Error for ', IDs{indID}]);
    end
end

end % A4_createPreprocStructure