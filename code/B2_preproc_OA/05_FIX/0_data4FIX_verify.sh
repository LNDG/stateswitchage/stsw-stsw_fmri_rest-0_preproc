#!/bin/bash

## FIX: File Preparation

# Rename the original filtered_func_data.nii.gz image and replace it with the preprocessed data. Will also attempt to take reg folder from FEAT+.feat if the default reg folder doesn't contain a highres image (necessary input for FIX).
source ../preproc_config.sh

# Preprocessing suffix. This denotes the preprocessing stage of the data, that is to say, the preprocessing steps which have already been undertaken before generating ICA.ica folder.
InputStage="feat_detrended_bandpassed"

# Test
#SubjectID="EYEMEMtest"

# Error Log
CurrentPreproc="data4FIX_verify"
CurrentLog="${LogPath}/05_FIX"; if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi
Error_Log="${CurrentLog}/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${Error_Log}

# Loop over participants, sessions (if they exist) & runs
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			fi

			FIX_Files="filtered_func_data.nii.gz filtered_func_data.ica/melodic_mix mc/prefiltered_func_data_mcf.par mask.nii.gz mean_func.nii.gz reg/example_func.nii.gz reg/highres.nii.gz reg/highres2example_func.mat design.fsf" 
			
			for FID in ${FIX_Files}; do
				if [ ! -f ${FuncPath}/FEAT.feat/${FID} ]; then
					echo "${FuncImage} missing ${FID}" >> ${Error_Log}	
					echo "${FuncImage} missing ${FID}"
				fi
			done
			
		done
	done
done