#!/bin/bash

source ../preproc_config.sh

if [ ! -d ${ScriptsPath}/extras ]; then mkdir -p ${ScriptsPath}/extras ; fi
ViewOutput="${ScriptsPath}/extras/ica_completion.txt"

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions" ; SessionFolder="" ; SessionName=""
	else Session="${SessionID}"
	fi	
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of the functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image

			# Verify if parent preprocessing file exists
			#if [ ! -f ${FuncPath}/${FuncImage}.nii.gz ]; then
			#	continue
			# Verify if ICA was run by checking if the ICA folder exists
			if [ ! -d ${FuncPath}/FEAT.feat/filtered_func_data.ica ]; then
				echo "${SUB} ${SessionName}${RUN} ICA NOT PERFORMED" >> ${ViewOutput}
				continue
			else
				ICADir="${FuncPath}/FEAT.feat/filtered_func_data.ica"
				echo -n "${SUB} ${SessionName}${RUN} " >> ${ViewOutput}
			fi
			
			# Verify ICA folders
			cd ${ICADir}
			if [ ! -d "${ICADir}/report" ]; then
				echo "MISSING REPORT FOLDER" >> ${ViewOutput}
				continue
			elif [ ! -d "${ICADir}/stats" ]; then
				echo "MISSING STATS FOLDER" >> ${ViewOutput}
				continue
			elif [ -d "${ICADir}/stats+" ]; then
				echo "$stats+ FOLDER FOUND - ICA RAN TWICE - VERIFY AND DELTE" >> ${ViewOutput}
				continue
			fi
			
			# Verify if log file finished as expected
			log=`sed '1!G;h;$!d' ${ICADir}/log.txt`
			if [ "${log:1:9}" != "finished!" ]; then
				echo "UEXPECTED LOG TERMINATION" >> ${ViewOutput}
				continue
			fi

			# Verify individual files
			Melodic_Files="eigenvalues_percent mean.nii.gz melodic_ICstats melodic_mix log.txt melodic_FTmix melodic_PPCA mask.nii.gz melodic_IC.nii.gz melodic_Tmodes"	
			for MF in ${Melodic_Files}; do
				if [ ! -f "${ICADir}/${MF}" ]; then
					echo "MISSING MELODIC FILE: ${MF}" >> ${ViewOutput}
					continue
				fi
			done
			Report_Files="00index.html EVplot.png head.html log.html nav.html"
			for RF in ${Report_Files}; do
				if [ ! -f "${ICADir}/report/${RF}" ]; then
					echo "MISSING REPORT FILE: ${RF}" >> ${ViewOutput}
					continue
				fi
			done
			if [ ! -f "${ICADir}/stats/stats.log" ]; then	
				echo "MISSING STATS FILE: stats.log" >> ${ViewOutput}
				continue
			fi
			
			# Count component report files
			cd ${ICADir}/report
			f_png=`find . -name 'f*.png' | wc -l`
			f_txt=`find . -name 'f*.txt' | wc -l`
			IC_html=`find . \( -name 'IC_?.html' -o -name 'IC_??.html' -o -name 'IC_???.html' \) | wc -l`
			IC_MMfit_png=`find . -name 'IC_*_MMfit.png' | wc -l`
			IC_MM_html=`find . -name 'IC_*_MM.html' | wc -l`
			IC_png=`find . \( -name 'IC_?.png' -o -name 'IC_??.png' -o -name 'IC_???.png' \) | wc -l`
			IC_prob_png=`find . -name 'IC_*_prob.png' | wc -l`
			IC_thresh_png=`find . -name 'IC_*_thresh.png' | wc -l`
			t_png=`find . -name 't*.png' | wc -l`
			t_txt=`find . -name 't*.txt' | wc -l`
			
			# Count component stats files
			cd ${ICADir}/stats
			MMstats=`find . -name 'MMstats*' | wc -l`
			probmap=`find . -name 'probmap*' | wc -l`
			thresh_zstat=`find . -name 'thresh_zstat*' | wc -l`
			
			# Find total component count
			Component_Count=`grep "IC map" ${ICADir}/log.txt | tail -1`
			Component_Count=`echo ${Component_Count:8}`
			Component_Count=`echo ${Component_Count%????}`

			# Verify if file counts match total component count
			IC_Files_Count="$f_png $f_txt $IC_html $IC_MMfit_png $IC_MM_html $IC_png $IC_prob_png $IC_thresh_png $t_png $t_txt $MMstats $probmap $thresh_zstat"
			st=0
			for ICFC in ${IC_Files_Count}; do
				[ ${Component_Count} = ${ICFC} ]
				st=$(( $? + st ))
			done
			if [ ! ${st} -eq 0 ]; then
				echo -n "FILE COUNTS DO NOT MATCH. FIRST MISMATCH COMES FROM " >> ${ViewOutput}
			fi
			
			# Verify if all necessary files exist for each component
			for IC in $(seq 1 ${Component_Count}); do 
				if [ ! -f ${ICADir}/report/f${IC}.png ]; then
					echo "MISSING REPORT FILE: f${IC}.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/f${IC}.txt ]; then
					echo "MISSING REPORT FILE: f${IC}.txt" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}.html ]; then
					echo "MISSING REPORT FILE: IC_${IC}.html" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}_MMfit.png ]; then
					echo "MISSING REPORT FILE: IC_${IC}_MMfit.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}_MM.html ]; then
					echo "MISSING REPORT FILE: IC_${IC}_MM.html" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}.png ]; then
					echo "MISSING REPORT FILE: IC_${IC}.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}_prob.png ]; then
					echo "MISSING REPORT FILE: IC_${IC}_prob.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/IC_${IC}_thresh.png ]; then
					echo "MISSING REPORT FILE: IC_${IC}_thresh.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/t${IC}.png ]; then
					echo "MISSING REPORT FILE: t${IC}.png" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/report/t${IC}.txt ]; then
					echo "MISSING REPORT FILE: t${IC}.txt" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/stats/MMstats_${IC} ]; then
					echo "MISSING STATS FILE: MMstats_${IC}" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/stats/probmap_${IC}.nii.gz ]; then
					echo "MISSING STATS FILE: probmap_${IC}.nii.gz" >> ${ViewOutput}
					continue
				elif [ ! -f ${ICADir}/stats/thresh_zstat${IC}.nii.gz ]; then
					echo "MISSING STATS FILE: thresh_zstat${IC}.nii.gz" >> ${ViewOutput}
					continue
				fi
			done
			
			echo "ICA is complete" >> ${ViewOutput}
			
		done
	done
done