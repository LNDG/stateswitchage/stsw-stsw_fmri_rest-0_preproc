#!/bin/bash

## Noise Component Rejection

# This will remove the components which have been selected for rejection by FIX.

source preproc_config.sh

# Preprocessing suffix. This denotes the preprocessing stage of the data, that is to say, the preprocessing steps which have already been undertaken before generating ICA.
InputStage="feat_detrended_bandpassed"
OutputStage="feat_detrended_bandpassed_manualdenoise"

# Number of subjects in test set
#Nsubjects=`echo ${TestSetID} | wc -w`

# Test
#SubjectID="1117";

# PBS Log Info
CurrentPreproc="Denoise"
CurrentLog="${LogPath}/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir -p ${CurrentLog}; fi

# Error Log
Error_Log="${CurrentLog}/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${Error_Log}

## Create string with all FEAT directories
# Loop over participants, sessions (if they exist) & runs
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			elif [ -f ${FuncPath}/${FuncImage}_${OutputStage}.nii.gz ]; then
				continue
			fi	
			
			# Create rejcomps.txt file for run/condition
			cd ${ScriptsPath}/05_FIX/rejcomps
			#Training="fix4melview_Training_${ProjectName}_N${Nsubjects}_thr${FixThreshold}.txt"
			#if [ -f ${Training} ]
			#	cat ${Training} | tail -1  > rejcomps.txt
			#else
			#	echo "${Training} does not exist, cannot extract component list." >> ${Error_Log}
			#	continue
			#fi
			
			# Gridwise
			echo "#PBS -N ${CurrentPreproc}_${FuncImage}" 			>> job # Job name 
			echo "#PBS -l walltime=1:00:00" 						>> job # Time until job is killed 
			echo "#PBS -l mem=8gb" 									>> job # Books 4gb RAM for the job 
			#echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
			echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 

			echo ". /etc/fsl/5.0/fsl.sh"							>> job # Set fsl environment 	
					
			# Variables for denoising
	
			Preproc="${FuncPath}/${FuncImage}_${InputStage}.nii.gz"									# Preprocessed data image
			Denoised="${FuncPath}/${FuncImage}_${OutputStage}.nii.gz"								# Denoised image
			Melodic="${FuncPath}/FEAT.feat/filtered_func_data.ica/melodic_mix"						# Location of ICA generated Melodic Mix
			Rejected=$(cat ${FuncImage}_rejcomps.txt)											    # List of components to be removed
			
			# Run fsl_regfilt command
			echo "fsl_regfilt -i ${Preproc} -o ${Denoised} -d ${Melodic} -f \"${Rejected}\""  		>> job
			
			# Change permissions
			echo "chmod 770 ${Denoised}"  															>> job
			
			# Error Log
			echo "Difference=\`cmp ${Preproc} ${Denoised}\`" >> job
			echo "if [ -z \${Difference} ]; then echo 'Denoising did not change the preprocessing image: ${FuncImage}' >> ${Error_Log}; fi" >> job
			
			qsub job
			rm job
			
		done
	done
done
