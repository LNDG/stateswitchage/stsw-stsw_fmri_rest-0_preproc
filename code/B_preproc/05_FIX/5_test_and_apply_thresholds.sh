#!/bin/bash

## FIX: Test & Apply Thresholding Values
# Apply cleanup with certain threshold on your Test Set; can be run locally (doesn't need much time)
# Threshold: the higher, the more components will be rejected; recommended: 20; we have used 50-60 

source ../preproc_config.sh

# Preprocessing suffix. This denotes the preprocessing stage of the data, that is to say, the preprocessing steps which have already been undertaken before generating ICA.ica folder.
InputStage="feat_detrended_bandpassed"

# Set to N when threshold has laready been set in the configuration file 
Evaluation="N"

# Test
#SubjectID="EYEMEMtest"

# Evaluation Subjects
	# EX: SubjectID="SUB001 SUB002"
SubjectID="1164 1172 1247"; Evaluation="Y"

# Number of subjects in test set
Nsubjects=`echo ${TestSetID} | wc -w`

# PBS Log Info
CurrentPreproc="Apply_Threshold"
CurrentLog="${LogPath}/05_FIX/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir -p ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Error Log
Error_Log="${LogPath}/05_FIX/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${Error_Log}

## Create string with all FEAT directories
# Loop over participants, sessions (if they exist) & runs
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	
	# Gridwise
	echo "#PBS -N ${CurrentPreproc}_${SUB}" 				>> job # Job name 
	echo "#PBS -l walltime=0:10:00" 						>> job # Time until job is killed 
	echo "#PBS -l mem=1gb" 									>> job # Books 4gb RAM for the job 
	#echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
	echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
	echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 
	
	# Initialize FIX
	
	echo "module load fsl_fix" 								>> job
	echo "source /etc/fsl/5.0/fsl.sh" 						>> job
	
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				continue
			fi

			# Test Thresholds
			Training_Object="${ScriptsPath}/05_FIX/Training_${ProjectName}_N${Nsubjects}.RData"
			Training_File="fix4melview_Training_${ProjectName}_N${Nsubjects}_thr${FixThreshold}.txt"
			if [ ${Evaluation} = "Y" ]; then 
				
				# Output: txt-file (fix4melview_Training_thr##.txt) with names of noise components
				# TODO: Once a satisfactory threshold has been determined, set the FixThreshold variable in the configuration file.
				# TODO: Make sure to comment out the Evaluation Subjects when applying threhold.
				
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 20" >> job
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 30" >> job
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 40" >> job
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 50" >> job
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 60" >> job
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} 70" >> job
				
			# Apply Threshold
			else 
				echo "fix -c ${FuncPath}/FEAT.feat ${Training_Object} ${FixThreshold}" 					>> job
				# Error Log
				echo "if [ ! -f ${FuncPath}/FEAT.feat/${Training_File} ];" 								>> job
				echo "then echo '${FuncImage}: ${Training_File} does not exist' >> ${Error_Log}; fi " 	>> job

			fi
			
		done
	done
	
	qsub job
	rm job
	
done
