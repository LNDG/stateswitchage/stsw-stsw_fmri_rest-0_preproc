#!/bin/bash

# 180124 JQK: added check for availability of hand_labels_noise.txt

## FIX: Training Data creation
# Create Training.Rdata from Training Set (subjects must already have hand_labels_noise.txt in their FEAT directories)

source ../preproc_config.sh

# Preprocessing suffix. This denotes the preprocessing stage of the data, that is to say, the preprocessing steps which have already been undertaken before generating ICA.ica folder.
InputStage="feat_detrended_bandpassed"

# Training Set
SubjectID="${TestSetID}"

# Test
#SubjectID="EYEMEMtest EYEMEM011"

# String with all FEAT folders
TrainingSet=""

# Error Log
CurrentPreproc="Create_Training_Data"
CurrentLog="${LogPath}/05_FIX"; if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi
Error_Log="${CurrentLog}/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${Error_Log}

## Create string with all FEAT directories
# Loop over participants, sessions (if they exist) & runs
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then
		Session="NoSessions"
		SessionFolder=""
		SessionName=""
	else
		Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then
				continue
			else
				SessionFolder="${SES}/"
				SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the functional image folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			if [ ! -f ${FuncPath}/${FuncImage}_${InputStage}.nii.gz ]; then
				echo "${FuncImage}_${InputStage}.nii.gz does not exist. Process will fail when attempting to create Rdata." >> ${Error_Log}
				continue
			fi
			
			if [ ! -f ${FuncPath}/FEAT.feat/hand_labels_noise.txt ]; then
				echo "hand_labels_noise.txt does not exist. Skipping this run/subject. Please CHECK!" >> ${Error_Log}
				continue
			fi
			
			FeatPath="${FuncPath}/FEAT.feat"
			
			TrainingSet="${TrainingSet}${FeatPath} "
			
		done
	done
done

# Number of subjects
Nsubjects=`echo $SubjectID | wc -w`

# Initialize FIX
module load fsl_fix
source /etc/fsl/5.0/fsl.sh

# Location where Training.Rdata file will be saved
cd ${ScriptsPath}/05_FIX

# Create Training file
echo "Creating Training_${ProjectName}_N${Nsubjects}"
fix -t Training_${ProjectName}_N${Nsubjects} ${TrainingSet}
