## StateSwitch MR Preprocessing Rest

1. Copy Raw DICOMs and Zip them to avoid file transfer of many tiny files

The zipping of DICOM files happens in the script in the task folder.
This step sometimes fails and creates faulty .zip folders. In that case, copy the data from the raw directory again. The same applies if the data in the raw folder were the wrong one (e.g. wrong image of a repeated acquisition). The goal is to have a zipped representation of the data to be processed. This avoids EVER working from the raw data and allows specification at this early phase of which images to select (e.g. if there were repeated acquisitions of the same data for any reason).

A0_copyDICOM

- Input: raw data from raw directory!
- Output: zipped DICOM

The zipping of DICOM files happens in the script in the task folder.

2. Copy entire preproc branch to tardis

My approach was to copy the entire branch to a subfolder in the tardis-STSW directory named ‘WIP_XXX’ to avoid overlap of processing stages.

Note that any script changes were done in the tardis script directories and they get transferred to the server in the last step.

A01_copyPreproc

3. Convert DICOM to NIFTI (tardis)

Use the MATLAB tool dicm2nii to convert the zipped directories to NIFTY. The function was altered to dicm2nii_json to automatically produce the associated .json files from the DICOM images to be BIDS-compliant. Naming convention was also chosen to be BIDS-compliant.

[This may be improved by using BIDS-associated Python tools.]

Note that this does NOT automatically crop images (cf.dcm2nii).

A1_dcm2nii
A2_dcm2niiCheck

- check that all files have been processed, if not rerun
A3_dcm2nii_rmTempFolders
- copy relevant files to superdirectory and remove potential temp folders

4. Create the required preproc structure

A4_createPreprocStructure

5. LNDG preprocessing
To prepare preprocessing, the following data/toolboxes have to exist:

- C_Standards   | B_data
- ICA-AROMA-master  | D_tools
- NIFTI_toolbox   | D_tools
- preprocessing_tools  | D_tools
- spm_detrend   | D_tools

You can of course choose a different structure, then you simply have to alter the paths in the preproc-setup.

B_preproc:

Now the real preprocessing begins. These scripts are executed on tardis and the directories are synced back to the server following preprocessing.

01_preproc_BET

Note that T1w images were NOT cropped prior to brain extraction. The extracted results were generally satisfactory however and non-brain residuals were located outside the functional FOV.

- Executed in the task data folder as T1w are identical
- Manual selection of best brain extraction fit
- Selected parameters are protocolled in X_documentation/STSWD_BET_Protocol.txt
- JQK: added corresponding binary brain mask outputs for potential later use

01_preproc_BET_ANTs

After the initial preprocessing run, I was unsatisfied with the final coregistrations, especially for the older adults. One of the reasons for this was the failure of FSL BET to remove the dura, therefore forcing the 12 DOF affine coregistration of the BET T1w to MNI to consider both the dura and the (in OAs pronounced) CSF. I therefore used ANTs to assess its performance relating to dura extraction. ANTs performed remarkably well for all subjects and achieved better results than FSL. I therefore used these BETs for the final coregistration. NOTE: The initial coregistration for ICA still remains done on the FSL BETs, but there is no principled reason to reject a more liberal inclusion of brain here IMHO, other than it potentially messing up the FIX procedure.

02_preproc_FEAT

Smoothing & motion correction:

- NO B0 bias correction (unwarping) as no fieldmaps are available
- MCFLIRT motion correction (intra-modal BOLD registration)
- Delete 12 initial volumes to reach BOLD steady-state
- Default 4D intensity normalization (see <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT/UserGuide> under Intensity normalization)
- 7mm smoothing kernel
- Create registration to MNI (for FIX only!)
o BBR cost function (DOF to main structural)
 Note that this option may not work very well without bias correction.
o DOF to MNI: 12, normal search, linear registration

03_preproc_detrend_filter_call

- 3rd order detrending: linear, quadratic and cubic
- .01 to .1 bandpass filter (8th order butterworth)
- Re-adds mean to SPM demeaned files

04_preproc_ICA

- MELODIC spatial ICA
- Model selection: Minimum Description Length (MDL) (see <http://www.fmrib.ox.ac.uk/datasets/techrep/tr02cb1/tr02cb1.pdf>)

05_FIX

FIX was NOT used, as it produced unsatisfying results. All resting state data were manually denoised.

06_preproc_registration_FLIRT_verify

The standard parameters for FLIRT were applied here, which are different (!) from those of the FLIRT step: 12 DOF correg for both BOLDT1w and T1wMNI. Registrations are concatenated to avoid double interpolation. Note that FSL recommends 6 DOF for within-subject registrations and indeed choosing 6 DOF improves registration results: MR Registration  (Web view). The 6 DOF option has not been applied yet.

Note regarding registration files:

The image files in the FEAT.feat and reg folders correspond to the initial FEAT-based coregistration and NOT the final coregistration (note that different parameters are used). The only images that show the final EPI to MNI space overlap (and only that) are located in the script directories (A_scripts/B_preproc/extras/registration/slicesdir/).

B2_preproc_OAs

Procedures were identical for OAs, but with cropped T1w inputs to BET. Note that to separate the FIX Training.RData files, we chose ‘STSWD_OA’ as study name here. The study name was not written on any image outputs, therefore their naming convention is identical to the YAs.

IMPORTANT UPDATES:

• Overwriting previous files: The initial version of analyses were run using data coregistered with 12DOF functional to structural using the FSL BET images. The script producing these files can be found at: /Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_rest/preproc/A_scripts/B_preproc/Z_archive/06_preproc_registration_FLIRT.sh